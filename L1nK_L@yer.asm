section .data
	start: db "Starting custom packet",10,0
	len1: equ $-start
	web: db "enter the ip address",10,0
	len2: equ $-web
	ttl: db 1
section .bss
	ip:	resb 32
	packet:	resb 90
section .text	
global _start
_start:
		push rbp
		mov rbp,rsp
		mov rax,41
		mov rdi,2
		mov rsi,3
		mov rdx,1; creating a new socket to filter out icmp packets easier..socket for recv
		syscall
		mov r14,rax
		mov rdi,17
		mov rsi,3
		mov rdx,0x0003;/usr/include/linux/if_ether.h in this case we use AF_PACKET and SOCK_RAW it really doesn't matter what we use here. socket for sending
		mov rax,41
		syscall
		mov r13,rax
			;ICMP LAYER
		mov rdi,rax
		mov rcx,0

		
j1:		
		push word 'gh'; these pushes are the data of the ICMP PACKET
		push word 'ef'
		push word 'cd'
		push word 'ab'
		inc rcx
		cmp rcx,8
		jnz j1
		push word 0xb7c9;checksum... went the lazy route and found it through wireshark the proper way to find checksum is shown below (ip)
		push word 0x0008; this format is found from (http://www.tcpipguide.com/free/t_ICMPCommonMessageFormatandDataEncapsulation.htm)
		mov r12,rsp
		
	
nxt:	mov rsp,r12; no need to create icmp over and over again so we continue from here
		
jr:		mov rdi,r13	; loading the file discriptor for sending the packet
		xor rcx,rcx
		xor rax,rax;check format from here (http://www.tcpipguide.com/free/t_IPDatagramGeneralFormat.htm)

		push word 0x64cf  
		mov ch, byte [rsp]
		mov cl, byte [rsp+1]
		add eax,ecx
		push word 0x3ad8;destination
		mov ch, byte [rsp]
		mov cl, byte [rsp+1]
		add eax,ecx 
		push word 0xf801
		mov ch, byte [rsp]
		mov cl, byte [rsp+1]
		add eax,ecx
		push word 0xa8c0;source
		mov ch, byte [rsp]
		mov cl, byte [rsp+1]
		add eax,ecx
		push word 0x0000;place holder for the checksum
		mov rbx,rsp
		push word 0x0100;protocol and ttl
		mov cl,[ttl]
		mov byte[rsp],cl;changing the value of ttl 
		inc byte [ttl]; incrementing the value of ttl
		mov ch, byte [rsp]
		mov cl, byte [rsp+1]
		add eax,ecx
		push word 0x0040;flags
		mov ch, byte [rsp]
		mov cl, byte [rsp+1]
		add eax,ecx
		push word 0x7777;identification
		mov ch, byte [rsp]
		mov cl, byte [rsp+1]
		add eax,ecx
		push word 0x4c00;total lenght of the packet from ip to icmp 
		mov ch, byte [rsp]
		mov cl, byte [rsp+1]
		add eax,ecx
		push word 0x0045; ipv4 and total length header divided by 4
		mov ch, byte [rsp]
		mov cl, byte [rsp+1]
		add eax,ecx
		mov cx,ax
		and eax,0xffff0000
		shr eax,16;the above steps is to find checksum using ones complement for each word
		add ax,cx
		neg ax
		add ax, 0xffff
		mov byte [rbx+1],al
		mov byte [rbx],ah
		
		
		
		
		push word 0x0008;Ethernet IPv4 type is specified here not above when created socket

		push word 0xbe46

		push word 0xe07e
		push word 0x3dd4;source mac address
		push word 0x69d1
		push word 0x6ed2
		push word 0xb60c; destination mac address (plz look up your routers mac will be updating soon for router auto look up mac address)
		mov rsi,rsp
		
		
		
		mov rdx,0x5a; how much bytes to send total from memory 
		push word 0
		push word 0x0000
		push word 0x0000
		push word 0x0000
		push word 0x0600
		push word 0
		push word 0x0000
		push word 0x0002;interface number
		push word 0x0800
		push word 0xd;socket_ll format (http://man7.org/linux/man-pages/man7/packet.7.html)
		mov r8,rsp
		mov r9,20; this is the size of socket_ll from memory
		mov rax,44
		syscall
		
		;polling section for timeouts 
		mov rax,r14
		
		push word 0
		push word 0x0001; /usr/include/bits/poll.h
		push word 0
		push word ax
		mov rdi,rsp;look up man page
		mov rsi, 1
		mov rdx,2000;2 secs wait time
		mov rax,7
		syscall
		cmp rax,0
		jz nxt
		
		
		
		
	;this is to recive the icmp packet	
		
		
		mov rdi,r14;load the fd for reciver socket
		mov rsi,packet
		mov rdx,90
		mov rax,45
		push 0
		push 0
		push 0
		push word 0
		mov r8,rsp
		mov r9,16
		xor r10,r10
		syscall
		
		mov rbx,packet
		add rbx, 0xc
		
		
		; below is the format for extracting the ip address from the recived packets
		
		xor r8,r8
		xor rax,rax
		xor rcx,rcx
j2:		mov r9,1
		dec rsp
		mov rdx,'.'
		mov byte[rsp],dl
		xor rdx,rdx
		mov cl, byte [rbx]
		mov al,cl
		mov rcx,10
j3:		xor rdx,rdx
		div rcx
		
		add rdx,0x30
		dec rsp
		mov byte [rsp], dl
		inc r9
		cmp rax,0
		jnz j3
		inc rbx
		inc r8
		cmp r8,4
		mov rdi,1
		mov rsi,rsp
		mov rdx,r9
		mov rax,1
		syscall
		jnz j2
		mov rdi,1
		push 10
		mov rsi,rsp
		mov rdx,1
		mov rax,1
		syscall		 
		xor rax,rax
		mov al,byte [packet+20];extracting the type 
		cmp al,0; if not ok increase ttl and send again 
		mov rsp,r12
		jnz jr
		
		mov rax,60
		syscall
		