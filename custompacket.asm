;useful link for debugging https://programming-review.com/linux-error-codes/
section .data
	start: db "Starting custom packet",10,0
	len1: equ $-start
	web: db "enter the ip address",10,0
	len2: equ $-web
section .bss
	ip: resb 32
section .text
global _start
_start:
		mov rdi,1
		mov rsi,start
		mov rdx,len1
		mov rax,1
		syscall
		mov rdi,1
		mov rsi,web
		mov rdx,len2
		mov rax,1
		syscall
		mov rdi,0
		mov rsi,ip
		mov rdx,32
		mov rax,0
		syscall
		mov rdi,1
		mov rsi,ip
		mov rdx,32
		mov rax,1		
		syscall
		mov rdi,2
		mov rsi,3
		mov rdx,1;http://www.iana.org/assignments/protocol-numbers/protocol-numbers.xhtml
		mov rax,41
		syscall
		push rbp
		mov rbp,rsp
		push word 0xacd9130e;socket.inet_aton("172.217.19.14")   
		push word 0
		
		mov rax,60
		syscall
		