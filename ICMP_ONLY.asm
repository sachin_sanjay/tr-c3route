section .data
	start: db "Starting custom packet",10,0
	len1: equ $-start
	web: db "enter the ip address",10,0
	len2: equ $-web
	ip1: dd 0x0100007f
	len3: equ $-ip1
	string: db"ZYXWVUTSRQPONMLKJIHGFEDCBAzyxwvutsrqponmlkjihgfe"
section .bss
	ip: resb 32
section .text	
global _start
_start:
		push rbp
		mov rbp,rsp
		mov rdi,1
		mov rsi,start
		mov rdx,len1
		mov rax,1
		syscall
		mov rdi,1
		mov rsi,web
		mov rdx,len2
		mov rax,1
		syscall
		mov rdi,0
		mov rsi,ip
		mov rdx,32
		mov rax,0
		syscall
		mov rdi,1
		mov rsi,ip
		mov rdx,32
		mov rax,1		
		syscall
		mov rdi,2
		mov rsi,3
		mov rdx,1;http://www.iana.org/assignments/protocol-numbers/protocol-numbers.xhtml
		mov rax,41
		syscall
			;ICMP LAYER
		mov rdi,rax
		mov rcx,0
j1:		
		push word 'gh'
		push word 'ef'
		push word 'cd'
		push word 'ab'
		inc rcx
		cmp rcx,8
		jnz j1
		push word 0xb7c9
		push word 0x0008; this format is found from (http://www.tcpipguide.com/free/t_ICMPCommonMessageFormatandDataEncapsulation.htm)
		mov rsi,rsp
		mov rdx,56
		push 0x0100007f
		push 0
		push 2
		mov r8,rsp
		mov r9,16
		mov rax,44
		syscall
		mov rax,60
		syscall
		