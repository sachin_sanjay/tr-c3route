# Tr@c3route

[SOURCES: raw ip from binghamton](https://www.cs.binghamton.edu/~steflik/cs455/rawip.txt)
***

**this is assembly code (x86_64) is used to mimic trace route at a very low level** 

equivalent python code

```
#!python
import sys
from scapy.all import *
def tracesite(websit):
	a=IP(dst=websit,ttl=1)/ICMP()
	t=1
	count=0
	while(t!=0):
		p=sr1(a,verbose=False)
		print(p[IP].src,"\trouter ",count)
		t=p[ICMP].type
		a[IP].ttl+=1
		count+=1
    		
    	
def main():
	if(len(sys.argv)>1):
		tracesite(sys.argv[1])
	else:
		print("enter the websites name as an argument")	
if __name__=="__main__":
	main()
```
***
# Final Release 
**L1nk_L@yer.asm is the final release**


![Scheme] (runtime.png)

***

## compilation instructions(<> indicates placeholders) 


* ```nasm -f elf64 <filename.asm> -o <anyname.o>```
* ```ld <anyname.o> -o <output>```
* ```sudo ./<output>```



